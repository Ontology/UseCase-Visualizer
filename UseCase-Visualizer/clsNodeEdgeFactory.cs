﻿using GraphMLConnector;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace UseCase_Visualizer
{
    public class clsNodeEdgeFactory
    {
        public List<clsGroupItem> GroupItems { get; private set; }
        public List<clsNodeItem> Nodes { get; private set; }
        public List<clsEdgeItem> Edges { get; private set; }

        private clsLocalConfig localConfig;

        public clsNodeEdgeFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
            
        }

        public clsNodeEdgeFactory(Globals globals)
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }
            Initialize();
        }
        private void Initialize()
        {
            Nodes = new List<clsNodeItem>();
            Edges = new List<clsEdgeItem>();
            GroupItems = new List<clsGroupItem>();
        }

        public clsOntologyItem CreateLists(List<clsObjectRel> useCaseToUseCaseList, List<clsUseCase> useCaseList)
        {
            GroupItems = useCaseToUseCaseList.GroupBy(useCase => new { useCase.ID_Object, useCase.Name_Object }).Select(grp => new clsGroupItem
            {
                GroupId = grp.Key.ID_Object,
                GroupName = grp.Key.Name_Object,
                GraphId = localConfig.Globals.NewGUID
            }).ToList();

            var removeItems = new List<clsGroupItem>();
            (from grpItem1 in GroupItems
             join useCaseToUseCase in useCaseToUseCaseList on grpItem1.GroupId equals useCaseToUseCase.ID_Object
             join grpItem2 in GroupItems on useCaseToUseCase.ID_Other equals grpItem2.GroupId
             select new { grpItem1, grpItem2 }).ToList().ForEach(grpItem =>
             {
                 if (grpItem.grpItem1.GroupItems == null)
                 {
                     grpItem.grpItem1.GroupItems = new List<clsGroupItem>();
                 }
                 if (!grpItem.grpItem1.GroupItems.Any(gitm => gitm == grpItem.grpItem2))
                 {
                     grpItem.grpItem1.GroupItems.Add(grpItem.grpItem2);
                     removeItems.Add(grpItem.grpItem2);
                 }
             });

            var useCases = (from useCase in useCaseList
                            join groupItem in GroupItems on useCase.IdUseCase equals groupItem.GroupId into groupItems
                            from groupItem in groupItems.DefaultIfEmpty()
                            where groupItem == null
                            select useCase).ToList();

            GroupItems.RemoveAll(grpItem => removeItems.Any(rmi => rmi == grpItem));

            useCases.ForEach(useCase =>
            {
                useCase.NodeItem = new clsNodeItem
                {
                    IdNode = useCase.IdUseCase,
                    NameNode = useCase.NameUseCase,
                    XmlTemplate = localConfig.OItem_object_graphml___uml__use_case
                };

                Nodes.Add(useCase.NodeItem);

                var primaryNodes = useCase.PrimaryActors.Select(primActr => new clsNodeItem
                {
                    IdNode = primActr.GUID,
                    NameNode = primActr.Name,
                    SVGSourceId = 1,
                    XmlTemplate = localConfig.OItem_object_graphml___uml__primary_actor
                }).ToList();

                Nodes.AddRange(primaryNodes);

                var secondaryNodes = useCase.SecondaryActors.Select(secActr => new clsNodeItem
                {
                    IdNode = secActr.GUID,
                    NameNode = secActr.Name,
                    SVGSourceId = 1,
                    XmlTemplate = localConfig.OItem_object_graphml___uml__secondary_actor
                });

                Nodes.AddRange(secondaryNodes);

                Edges.AddRange(primaryNodes.Select(actr => new clsEdgeItem
                {
                    NodeItem2 = useCase.NodeItem,
                    NodeItem1 = actr,
                    OItem_RelationType = localConfig.OItem_relationtype_primary,
                    XmlTemplate = localConfig.OItem_object_graphml___uml_edge_without_label
                }));

                Edges.AddRange(secondaryNodes.Select(actr => new clsEdgeItem
                {
                    NodeItem2 = useCase.NodeItem,
                    NodeItem1 = actr,
                    OItem_RelationType = localConfig.OItem_relationtype_primary,
                    XmlTemplate = localConfig.OItem_object_graphml___uml_edge_without_label
                }));

                Edges.AddRange(from useCaseRel in useCaseToUseCaseList
                               join useCasePar in useCases on useCaseRel.ID_Object equals useCasePar.IdUseCase
                               join useCaseChild in useCases on useCaseRel.ID_Other equals useCaseChild.IdUseCase
                               select new clsEdgeItem
                               {
                                   NodeItem1 = useCasePar.NodeItem,
                                   NodeItem2 = useCaseChild.NodeItem,
                                   OItem_RelationType = localConfig.OItem_relationtype_contains,
                                   XmlTemplate = localConfig.OItem_object_graphml___uml_edge_without_label
                               });

                GroupItems.ForEach(grpItem =>
                {
                    grpItem.Nodes = (from rel in useCaseToUseCaseList.Where(relItem => relItem.ID_Object == grpItem.GroupId)
                                     join node in Nodes on rel.ID_Other equals node.IdNode
                                     select node).ToList();
                    grpItem.Edges = (from edge in Edges
                                     join node1 in grpItem.Nodes on edge.NodeItem1 equals node1
                                     join node2 in grpItem.Nodes on edge.NodeItem2 equals node2
                                     select edge).ToList();

                });

                removeItems.ForEach(grpItem =>
                {
                    grpItem.Nodes = (from rel in useCaseToUseCaseList.Where(relItem => relItem.ID_Object == grpItem.GroupId)
                                     join node in Nodes on rel.ID_Other equals node.IdNode
                                     select node).ToList();
                    grpItem.Edges = (from edge in Edges
                                     join node1 in grpItem.Nodes on edge.NodeItem1 equals node1
                                     join node2 in grpItem.Nodes on edge.NodeItem2 equals node2
                                     select edge).ToList();
                });
            });

            return localConfig.Globals.LState_Success.Clone();
        }

        
    }
}
