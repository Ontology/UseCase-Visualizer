﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UseCase_Visualizer
{
    public  class clsDataWork_USECase
    {
        private clsLocalConfig localConfig;
        private OntologyModDBConnector dbConnector_Projects;
        private OntologyModDBConnector dbConnector_Projects_ProjectResources;
        private OntologyModDBConnector dbConnector_ProjectResources_UseCases;
        private OntologyModDBConnector dbConnector_SoftwareProjects;
        private OntologyModDBConnector dbConnector_UseCases;
        private OntologyModDBConnector dbConnector_Actors;
        
        public List<clsOntologyItem> SoftwareProjects { get; set; }

        public List<clsObjectRel> UseCasesOfSoftwareProjects { get; set; }

        public List<clsUseCase> UseCaseList { get; set; }
        public List<clsObjectRel> UseCaseToUseCase { get; set; }

        public List<TreeNode> RootNodes { get; set; }
        public List<TreeNode> ProjectNodes { get; set; }

        public List<ProjectToSoftwareProject> SoftwareProjectToProject { get; set; }

        public clsOntologyItem GraphML_UML_ActorSource
        {
            get { return localConfig.OItem_object_graphml___uml__actor_source; }
        }

        public List<clsObjectRel> ProjectHierarchy
        {
            get { return dbConnector_Projects.ObjectRels; }
        }

        public List<clsObjectRel> ProjectResourcesOfProjects
        {
            get { return dbConnector_Projects_ProjectResources.ObjectRels; }
        }

        public List<clsObjectRel> UseCasesOfProjectResources
        {
            get { return dbConnector_ProjectResources_UseCases.ObjectRels; }
        }

        public List<TreeNode> GetProjectNodes()
        {
            var projects = dbConnector_Projects.ObjectRels.GroupBy(projh => new { GUID = projh.ID_Object, Name = projh.Name_Object, GUID_Parent = projh.ID_Parent_Object });

            RootNodes = (from project in projects
                             join parProject in dbConnector_Projects.ObjectRels on project.Key.GUID equals parProject.ID_Other into parProjects
                             from parProject in parProjects.DefaultIfEmpty()
                             where parProject == null
                             select new TreeNode()
                             {
                                 Name = project.Key.GUID,
                                 Text = project.Key.Name,
                                 ImageIndex = localConfig.ImageId_Project,
                                 SelectedImageIndex = localConfig.ImageId_Project
                             }).ToList();

            RootNodes.ForEach(node => GetSubNodes(node));

            if (ProjectNodes == null)
            {
                ProjectNodes = new List<TreeNode>();
            }
            ProjectNodes.AddRange(from rootNode in RootNodes
                            join projectResource in dbConnector_Projects_ProjectResources.ObjectRels on rootNode.Name equals projectResource.ID_Object
                            select rootNode);

            return RootNodes;

            
        }

        private void GetSubNodes(TreeNode parentNode)
        {
            var subNodes = dbConnector_Projects.ObjectRels.Where(prj => prj.ID_Object == parentNode.Name).Select(prj => new TreeNode()
                             {
                                 Name = prj.ID_Other,
                                 Text = prj.Name_Other,
                                 ImageIndex = localConfig.ImageId_Project,
                                 SelectedImageIndex = localConfig.ImageId_Project
                             }).ToList();
            parentNode.Nodes.AddRange(subNodes.ToArray());

            if (ProjectNodes == null)
            {
                ProjectNodes = new List<TreeNode>();
            }
            ProjectNodes.AddRange(from subNode in subNodes
                                  join projectResource in dbConnector_Projects_ProjectResources.ObjectRels on subNode.Name equals projectResource.ID_Other
                            select subNode);

            subNodes.ForEach(subNode => GetSubNodes(subNode));
                
        }

        public clsOntologyItem GetData_ProjectOfUseCases()
        {
            var searchProjectResources = new List<clsObjectRel> {new clsObjectRel
            {
                ID_Parent_Other = localConfig.OItem_class_use_case.GUID,
                ID_RelationType = localConfig.OItem_relationtype_belonging_resource.GUID,
                ID_Parent_Object = localConfig.OItem_class_project_resources.GUID
            }};

            var result = dbConnector_ProjectResources_UseCases.GetDataObjectRel(searchProjectResources);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var searchProjectsOfResources = dbConnector_ProjectResources_UseCases.ObjectRels.Select(prjr => new clsObjectRel
                    {
                        ID_Object = prjr.ID_Object,
                        ID_RelationType = localConfig.OItem_relationtype_belongs_to.GUID,
                        ID_Parent_Other = localConfig.OItem_class_project.GUID
                    }).ToList();

                if (searchProjectsOfResources.Any())
                {
                    result = dbConnector_Projects_ProjectResources.GetDataObjectRel(searchProjectsOfResources);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        var searchProjectHierarchy = dbConnector_Projects_ProjectResources.ObjectRels.Select(prj => new clsObjectRel
                        {
                            ID_Other = prj.ID_Other,
                            ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                            ID_Parent_Object = localConfig.OItem_class_project.GUID
                        }).ToList();

                        if (searchProjectHierarchy.Any())
                        {
                            result = dbConnector_Projects.GetDataObjectRel(searchProjectHierarchy);
                            
                        }
                        
                    }
                }

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    GetSoftwareProjectsOfProjects();
                }
                
            }

            return result;
        }

        public void GetSoftwareProjectsOfProjects()
        {
            SoftwareProjectToProject = (from ucToSPrj in UseCasesOfSoftwareProjects
                                        join ucToPrjR in dbConnector_ProjectResources_UseCases.ObjectRels on ucToSPrj.ID_Other equals ucToPrjR.ID_Other
                                        join prjRToPrj in dbConnector_Projects_ProjectResources.ObjectRels on ucToPrjR.ID_Object equals prjRToPrj.ID_Object
                                        select new { prjRToPrj, ucToSPrj }).GroupBy(prj => new
                        {
                            IdProject = prj.prjRToPrj.ID_Other,
                            IdSoftwareProject = prj.ucToSPrj.ID_Object,
                            NameSoftwareProject = prj.ucToSPrj.Name_Object
                        }).Select(prj => new ProjectToSoftwareProject
                        {
                            IdProject = prj.Key.IdProject,
                            IdSoftwareProject = prj.Key.IdSoftwareProject,
                            NameSoftwareProject = prj.Key.NameSoftwareProject
                        }).ToList();
                        
        }

        public clsOntologyItem GetData_SoftwareProjects(bool loadActors = true)
        {
            var searchSoftwareProjects = new List<clsObjectRel> {new clsObjectRel
            {
                ID_Parent_Object = localConfig.OItem_class_software_project.GUID,
                ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Other = localConfig.OItem_class_use_case.GUID
            }};

            var result = dbConnector_SoftwareProjects.GetDataObjectRel(searchSoftwareProjects);

            SoftwareProjects = new List<clsOntologyItem>();

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                SoftwareProjects = dbConnector_SoftwareProjects.ObjectRels.GroupBy(prjs => new {GUID = prjs.ID_Object, Name = prjs.Name_Object }).Select(prjs => new clsOntologyItem
                    {
                        GUID = prjs.Key.GUID,
                        Name = prjs.Key.Name,
                        GUID_Parent = localConfig.OItem_class_software_project.GUID,
                        Type = localConfig.Globals.Type_Object
                    }).OrderBy(prjs => prjs.Name).ToList();

                UseCasesOfSoftwareProjects = dbConnector_SoftwareProjects.ObjectRels.OrderBy(usec => usec.ID_Object).ThenBy(usec => usec.Name_Other).ToList();

                UseCaseList = UseCasesOfSoftwareProjects.Select(useCase =>
                        new clsUseCase
                        {
                            IdUseCase = useCase.ID_Other,
                            NameUseCase = useCase.Name_Other,
                            UseCasePath = useCase.ID_Object + useCase.ID_Other
                        }).ToList();

                result = GetSubData_SubUseCases();

                if (loadActors && result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    result = GetSubData_Actors();
                }
            }

            return result;
        }

        private clsOntologyItem GetSubData_SubUseCases()
        {
            var count = 0;
            var result = localConfig.Globals.LState_Success.Clone();
            do
            {
                count = UseCaseList.Count;
                var searchUseCases = UseCaseList.Select(useCase =>
                        new clsObjectRel
                        {
                            ID_Object = useCase.IdUseCase,
                            ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                            ID_Parent_Other = localConfig.OItem_class_use_case.GUID
                        }).ToList();

                if (searchUseCases.Any())
                {
                    result = dbConnector_UseCases.GetDataObjectRel(searchUseCases);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        UseCaseToUseCase = dbConnector_UseCases.ObjectRels;
                        var newUseCases = (from useCase in dbConnector_UseCases.ObjectRels
                                           join useCaseOld in UseCaseList on useCase.ID_Other equals useCaseOld.IdUseCase into useCasesOld
                                           from useCaseOld in useCasesOld.DefaultIfEmpty()
                                           join useCaseParent in UseCaseList on useCase.ID_Object equals useCaseParent.IdUseCase
                                           where useCaseOld == null
                                           select new clsUseCase
                                           {
                                               IdUseCase = useCase.ID_Other,
                                               NameUseCase = useCase.Name_Other,
                                               UseCasePath = useCaseParent.UseCasePath + useCase.ID_Other
                                           });

                        if (newUseCases.Any())
                        {
                            UseCaseList.AddRange(newUseCases);
                        }
                    }
                }
              

            }while (count < UseCaseList.Count);

            return result;
        }

        public clsOntologyItem GetSubData_Actors()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var searchActors = UseCaseList.Select(useCase =>
                new clsObjectRel
                {
                    ID_Object = useCase.IdUseCase,
                    ID_Parent_Other = localConfig.OItem_class_actor.GUID
                }).ToList();

            if (searchActors.Any())
            {
                result = dbConnector_Actors.GetDataObjectRel(searchActors);

                if (result.GUID == localConfig.Globals.LState_Success.GUID && dbConnector_Actors.ObjectRels.Any())
                {
                    UseCaseList.ForEach(useCase =>
                        {
                            useCase.PrimaryActors = dbConnector_Actors.ObjectRels.Where(act => act.ID_Object == useCase.IdUseCase
                                && act.ID_RelationType == localConfig.OItem_relationtype_primary.GUID).Select(act => new clsOntologyItem
                                {
                                    GUID = act.ID_Other,
                                    Name = act.Name_Other,
                                    GUID_Parent = localConfig.OItem_class_actor.GUID,
                                    Type = localConfig.Globals.Type_Object
                                }).ToList();

                            useCase.SecondaryActors = dbConnector_Actors.ObjectRels.Where(act => act.ID_Object == useCase.IdUseCase
                                && act.ID_RelationType == localConfig.OItem_relationtype_secondary.GUID).Select(act => new clsOntologyItem
                                {
                                    GUID = act.ID_Other,
                                    Name = act.Name_Other,
                                    GUID_Parent = localConfig.OItem_class_actor.GUID,
                                    Type = localConfig.Globals.Type_Object
                                }).ToList();
                        });
                }
            }


            return result;
        }


        public clsDataWork_USECase(Globals globals)
        {
            this.localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (this.localConfig == null)
            {
                this.localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(this.localConfig);
            }

            Initialize();

        }

        private void Initialize()
        {
            dbConnector_SoftwareProjects = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_UseCases = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_Actors = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_Projects = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_ProjectResources_UseCases = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_Projects_ProjectResources = new OntologyModDBConnector(localConfig.Globals);
        }

        public clsDataWork_USECase(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;


            Initialize();
        }

    }
}
