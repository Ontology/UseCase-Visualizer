﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCase_Visualizer
{
    public class ProjectToSoftwareProject
    {
        public string IdSoftwareProject { get; set; }
        public string NameSoftwareProject { get; set; }
        public string IdProject { get; set; }
    }
}
