﻿using GraphMLConnector;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UseCase_Visualizer
{
    public partial class UseCaseVusualizer : Form
    {
        private clsLocalConfig objLocalConfig;
        private clsDataWork_USECase dataWork_useCase;
        private clsNodeEdgeFactory nodeEdgeFactory;
        private clsGraphMLWork graphMLWork;
        private TreeNode treeNode_Root;
        private TreeNode treeNode_LastProject;
        private int currentIxUseCase;

        public UseCaseVusualizer()
        {
            InitializeComponent();

            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            dataWork_useCase = new clsDataWork_USECase(objLocalConfig);
            graphMLWork = new clsGraphMLWork(objLocalConfig.Globals);
            nodeEdgeFactory = new clsNodeEdgeFactory(objLocalConfig);
            FillTree();
        }

        private void FillTree()
        {
            timer_UseCaseNodes.Stop();
            treeView_SoftwareProjects.Nodes.Clear();

            treeNode_Root = treeView_SoftwareProjects.Nodes.Add(objLocalConfig.Globals.Root.GUID, objLocalConfig.Globals.Root.Name, objLocalConfig.ImageId_Root, objLocalConfig.ImageId_Root);

            
            var result = dataWork_useCase.GetData_SoftwareProjects();
            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                
                
                result = dataWork_useCase.GetData_ProjectOfUseCases();
                if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    var rootNodes = dataWork_useCase.GetProjectNodes();
                    rootNodes.OrderBy(rn => rn.Name).ToList().ForEach(rn =>
                        {
                            treeNode_Root.Nodes.Add(rn);
                        });

                    var projectNodes = dataWork_useCase.ProjectNodes;

                    (from prjN in projectNodes
                     join sPrjToPrj in dataWork_useCase.SoftwareProjectToProject on prjN.Name equals sPrjToPrj.IdProject
                     select new { prjN, sPrjToPrj }).ToList().ForEach(prj =>
                     {
                         prj.prjN.Nodes.Add(prj.sPrjToPrj.IdSoftwareProject, prj.sPrjToPrj.NameSoftwareProject, objLocalConfig.ImageId_SoftwareProject, objLocalConfig.ImageId_SoftwareProject);
                     });
                    
                    timer_UseCaseNodes.Start();
                }
                else
                {
                    MessageBox.Show(this, "Die Softwareprojekte mit zugehörigen UseCases konnte nicht ermittelt werden!", "Fehler!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
            {
                MessageBox.Show(this, "Die Softwareprojekte mit zugehörigen UseCases konnte nicht ermittelt werden!", "Fehler!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void toolStripButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer_UseCaseNodes_Tick(object sender, EventArgs e)
        {
            if (currentIxUseCase < dataWork_useCase.UseCasesOfSoftwareProjects.Count)
            {
                toolStripProgressBar_LoadUseCases.Value = 50;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                while (sw.ElapsedMilliseconds < 200 && currentIxUseCase < dataWork_useCase.UseCasesOfSoftwareProjects.Count)
                {
                    var idProject = dataWork_useCase.UseCasesOfSoftwareProjects[currentIxUseCase].ID_Object;

                    if (treeNode_LastProject != null)
                    {
                        if (treeNode_LastProject.Name != idProject)
                        {
                            var treeNodes = treeNode_Root.Nodes.Find(idProject, true);
                            treeNode_LastProject = treeNodes[0];
                        }
                    }
                    else
                    {
                        var treeNodes = treeNode_Root.Nodes.Find(idProject, true);
                        if (treeNodes.Any())
                        {
                            treeNode_LastProject = treeNodes[0];
                        }
                        
                    }

                    if (treeNode_LastProject != null)
                    {
                        treeNode_LastProject.Nodes.Add(dataWork_useCase.UseCasesOfSoftwareProjects[currentIxUseCase].ID_Other, dataWork_useCase.UseCasesOfSoftwareProjects[currentIxUseCase].Name_Other, objLocalConfig.ImageId_UseCase, objLocalConfig.ImageId_UseCase);
                        currentIxUseCase++;
                    }
                    
                }
                sw.Stop();
            }
            else
            {
                toolStripProgressBar_LoadUseCases.Value = 0;
                timer_UseCaseNodes.Stop();
                toolStripLabel_SoftwareProjects.Text = dataWork_useCase.SoftwareProjects.Count.ToString();
                toolStripLabel_UseCasesCount.Text = dataWork_useCase.UseCasesOfSoftwareProjects.Count.ToString();
            }
        }

        private void contextMenuStrip_UseCases_Opening(object sender, CancelEventArgs e)
        {
            createGraphMLToolStripMenuItem.Enabled = false;
            var treeNode_Selected = treeView_SoftwareProjects.SelectedNode;

            if (treeNode_Selected != null)
            {
                if (treeNode_Selected.ImageIndex == objLocalConfig.ImageId_SoftwareProject)
                {
                    createGraphMLToolStripMenuItem.Enabled = true;
                }
            }
        }

        private void createGraphMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var treeNode_Selected = treeView_SoftwareProjects.SelectedNode;

            if (treeNode_Selected != null)
            {
                if (saveFileDialog_Export.ShowDialog(this) ==System.Windows.Forms.DialogResult.OK)
                {
                    //var groupUseCases = dataWork_useCase.UseCaseToUseCase.GroupBy(useCase => new { useCase.ID_Object, useCase.Name_Object }).Select(grp => new clsGroupItem
                    //{
                    //    GroupId = grp.Key.ID_Object,
                    //    GroupName = grp.Key.Name_Object,
                    //    GraphId = objLocalConfig.Globals.NewGUID
                    //}).ToList();

                    //var removeItems = new List<clsGroupItem>();
                    //(from grpItem1 in groupUseCases
                    //                 join useCaseToUseCase in dataWork_useCase.UseCaseToUseCase on grpItem1.GroupId equals useCaseToUseCase.ID_Object
                    //                 join grpItem2 in groupUseCases on useCaseToUseCase.ID_Other equals grpItem2.GroupId 
                    //                 select new {grpItem1, grpItem2}).ToList().ForEach( grpItem =>
                    //                 {
                    //                     if (grpItem.grpItem1.GroupItems == null)
                    //                     {
                    //                         grpItem.grpItem1.GroupItems = new List<clsGroupItem>();
                    //                     }
                    //                     if (!grpItem.grpItem1.GroupItems.Any(gitm => gitm == grpItem.grpItem2))
                    //                     {
                    //                         grpItem.grpItem1.GroupItems.Add(grpItem.grpItem2);
                    //                         removeItems.Add(grpItem.grpItem2);
                    //                     }
                    //                 });

                    

                    //var useCases = (from useCase in dataWork_useCase.UseCaseList
                    //                join groupItem in groupUseCases on useCase.IdUseCase equals groupItem.GroupId into groupItems
                    //                from groupItem in groupItems.DefaultIfEmpty()
                    //                where groupItem == null
                    //                select useCase).ToList();

                    //groupUseCases.RemoveAll(grpItem => removeItems.Any(rmi => rmi == grpItem));
                    

                    //List<clsEdgeItem> edges = new List<clsEdgeItem>();
                    //List<clsNodeItem> nodes = new List<clsNodeItem>();

                    //useCases.ForEach(useCase =>
                    //{
                    //    useCase.NodeItem = new clsNodeItem
                    //    {
                    //        IdNode = useCase.IdUseCase,
                    //        NameNode = useCase.NameUseCase,
                    //        XmlTemplate = objLocalConfig.OItem_object_graphml___uml__use_case
                    //    };

                    //    nodes.Add(useCase.NodeItem);

                    //    var primaryNodes = useCase.PrimaryActors.Select(primActr => new clsNodeItem
                    //    {
                    //        IdNode = primActr.GUID,
                    //        NameNode = primActr.Name,
                    //        SVGSourceId = 1,
                    //        XmlTemplate = objLocalConfig.OItem_object_graphml___uml__primary_actor
                    //    }).ToList();

                    //    nodes.AddRange(primaryNodes);

                    //    var secondaryNodes = useCase.SecondaryActors.Select(secActr => new clsNodeItem
                    //    {
                    //        IdNode = secActr.GUID,
                    //        NameNode = secActr.Name,
                    //        SVGSourceId = 1,
                    //        XmlTemplate = objLocalConfig.OItem_object_graphml___uml__secondary_actor
                    //    });

                    //    nodes.AddRange(secondaryNodes);

                    //    edges.AddRange(primaryNodes.Select(actr => new clsEdgeItem
                    //    {
                    //        NodeItem2 = useCase.NodeItem,
                    //        NodeItem1 = actr,
                    //        OItem_RelationType = objLocalConfig.OItem_relationtype_primary,
                    //        XmlTemplate = objLocalConfig.OItem_object_graphml___uml_edge_without_label
                    //    }));

                    //    edges.AddRange(secondaryNodes.Select(actr => new clsEdgeItem
                    //    {
                    //        NodeItem2 = useCase.NodeItem,
                    //        NodeItem1 = actr,
                    //        OItem_RelationType = objLocalConfig.OItem_relationtype_primary,
                    //        XmlTemplate = objLocalConfig.OItem_object_graphml___uml_edge_without_label
                    //    }));
                    //});

                    //edges.AddRange(from useCaseRel in dataWork_useCase.UseCaseToUseCase
                    //               join useCasePar in useCases on useCaseRel.ID_Object equals useCasePar.IdUseCase
                    //               join useCaseChild in useCases on useCaseRel.ID_Other equals useCaseChild.IdUseCase
                    //               select new clsEdgeItem
                    //               {
                    //                   NodeItem1 = useCasePar.NodeItem,
                    //                   NodeItem2 = useCaseChild.NodeItem,
                    //                   OItem_RelationType = objLocalConfig.OItem_relationtype_contains,
                    //                   XmlTemplate = objLocalConfig.OItem_object_graphml___uml_edge_without_label
                    //               });



                    //groupUseCases.ForEach(grpItem =>
                    //    {
                    //        grpItem.Nodes = (from rel in dataWork_useCase.UseCaseToUseCase.Where(relItem => relItem.ID_Object == grpItem.GroupId)
                    //                     join node in nodes on rel.ID_Other equals node.IdNode
                    //                     select node).ToList();
                    //        grpItem.Edges = (from edge in edges
                    //                         join node1 in grpItem.Nodes on edge.NodeItem1 equals node1
                    //                         join node2 in grpItem.Nodes on edge.NodeItem2 equals node2
                    //                         select edge).ToList();

                    //    });

                    //removeItems.ForEach(grpItem =>
                    //    {
                    //        grpItem.Nodes = (from rel in dataWork_useCase.UseCaseToUseCase.Where(relItem => relItem.ID_Object == grpItem.GroupId)
                    //                         join node in nodes on rel.ID_Other equals node.IdNode
                    //                         select node).ToList();
                    //        grpItem.Edges = (from edge in edges
                    //                         join node1 in grpItem.Nodes on edge.NodeItem1 equals node1
                    //                         join node2 in grpItem.Nodes on edge.NodeItem2 equals node2
                    //                         select edge).ToList();
                    //    });

                    var result = nodeEdgeFactory.CreateLists(dataWork_useCase.UseCaseToUseCase, dataWork_useCase.UseCaseList);

                    if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        graphMLWork.ExportGraph(saveFileDialog_Export.FileName,
                                            nodeEdgeFactory.Nodes,
                                            nodeEdgeFactory.Edges,
                                            nodeEdgeFactory.GroupItems,
                                            new List<clsOntologyItem> { dataWork_useCase.GraphML_UML_ActorSource });
                    }
                    else
                    {
                        MessageBox.Show(this, "Bei der Ermittlung der Elemente für die Graph-Erzeugung ist ein Fehler aufgetreten!", "Fehler!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                    
                    
                }
                
            }
        }
    }
}
