﻿using GraphMLConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UseCase_Visualizer
{
    public class clsUseCase
    {
        public string IdUseCase { get; set; }
        public string NameUseCase { get; set; }
        public string UseCasePath { get; set; }
        public clsNodeItem NodeItem { get; set; }
        public List<clsOntologyItem> PrimaryActors { get; set; }
        public List<clsOntologyItem> SecondaryActors { get; set; }
    }
}
