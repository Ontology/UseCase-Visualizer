﻿namespace UseCase_Visualizer
{
    partial class UseCaseVusualizer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UseCaseVusualizer));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton_Close = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.toolStripContainer2 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel_SoftwareProjectsLBL = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel_SoftwareProjects = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel_UseCasesCountLbl = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel_UseCasesCount = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripProgressBar_LoadUseCases = new System.Windows.Forms.ToolStripProgressBar();
            this.treeView_SoftwareProjects = new System.Windows.Forms.TreeView();
            this.contextMenuStrip_UseCases = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createGraphMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList_UseCases = new System.Windows.Forms.ImageList(this.components);
            this.timer_UseCaseNodes = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog_Export = new System.Windows.Forms.SaveFileDialog();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStripContainer2.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer2.ContentPanel.SuspendLayout();
            this.toolStripContainer2.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.contextMenuStrip_UseCases.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(943, 528);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(943, 578);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton_Close});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(62, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // toolStripButton_Close
            // 
            this.toolStripButton_Close.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_Close.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_Close.Image")));
            this.toolStripButton_Close.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_Close.Name = "toolStripButton_Close";
            this.toolStripButton_Close.Size = new System.Drawing.Size(50, 22);
            this.toolStripButton_Close.Text = "x_Close";
            this.toolStripButton_Close.Click += new System.EventHandler(this.toolStripButton_Close_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.toolStripContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(943, 528);
            this.splitContainer1.SplitterDistance = 389;
            this.splitContainer1.TabIndex = 0;
            // 
            // toolStripContainer2
            // 
            // 
            // toolStripContainer2.BottomToolStripPanel
            // 
            this.toolStripContainer2.BottomToolStripPanel.Controls.Add(this.toolStrip2);
            // 
            // toolStripContainer2.ContentPanel
            // 
            this.toolStripContainer2.ContentPanel.Controls.Add(this.treeView_SoftwareProjects);
            this.toolStripContainer2.ContentPanel.Size = new System.Drawing.Size(385, 474);
            this.toolStripContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer2.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer2.Name = "toolStripContainer2";
            this.toolStripContainer2.Size = new System.Drawing.Size(385, 524);
            this.toolStripContainer2.TabIndex = 0;
            this.toolStripContainer2.Text = "toolStripContainer2";
            // 
            // toolStrip2
            // 
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel_SoftwareProjectsLBL,
            this.toolStripLabel_SoftwareProjects,
            this.toolStripSeparator1,
            this.toolStripLabel_UseCasesCountLbl,
            this.toolStripLabel_UseCasesCount,
            this.toolStripSeparator2,
            this.toolStripProgressBar_LoadUseCases});
            this.toolStrip2.Location = new System.Drawing.Point(3, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(337, 25);
            this.toolStrip2.TabIndex = 0;
            // 
            // toolStripLabel_SoftwareProjectsLBL
            // 
            this.toolStripLabel_SoftwareProjectsLBL.Name = "toolStripLabel_SoftwareProjectsLBL";
            this.toolStripLabel_SoftwareProjectsLBL.Size = new System.Drawing.Size(113, 22);
            this.toolStripLabel_SoftwareProjectsLBL.Text = "x_Software-Projects:";
            // 
            // toolStripLabel_SoftwareProjects
            // 
            this.toolStripLabel_SoftwareProjects.Name = "toolStripLabel_SoftwareProjects";
            this.toolStripLabel_SoftwareProjects.Size = new System.Drawing.Size(13, 22);
            this.toolStripLabel_SoftwareProjects.Text = "0";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel_UseCasesCountLbl
            // 
            this.toolStripLabel_UseCasesCountLbl.Name = "toolStripLabel_UseCasesCountLbl";
            this.toolStripLabel_UseCasesCountLbl.Size = new System.Drawing.Size(72, 22);
            this.toolStripLabel_UseCasesCountLbl.Text = "x_Use Cases:";
            // 
            // toolStripLabel_UseCasesCount
            // 
            this.toolStripLabel_UseCasesCount.Name = "toolStripLabel_UseCasesCount";
            this.toolStripLabel_UseCasesCount.Size = new System.Drawing.Size(13, 22);
            this.toolStripLabel_UseCasesCount.Text = "0";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripProgressBar_LoadUseCases
            // 
            this.toolStripProgressBar_LoadUseCases.Name = "toolStripProgressBar_LoadUseCases";
            this.toolStripProgressBar_LoadUseCases.Size = new System.Drawing.Size(100, 22);
            // 
            // treeView_SoftwareProjects
            // 
            this.treeView_SoftwareProjects.ContextMenuStrip = this.contextMenuStrip_UseCases;
            this.treeView_SoftwareProjects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView_SoftwareProjects.ImageIndex = 0;
            this.treeView_SoftwareProjects.ImageList = this.imageList_UseCases;
            this.treeView_SoftwareProjects.Location = new System.Drawing.Point(0, 0);
            this.treeView_SoftwareProjects.Name = "treeView_SoftwareProjects";
            this.treeView_SoftwareProjects.SelectedImageIndex = 0;
            this.treeView_SoftwareProjects.Size = new System.Drawing.Size(385, 474);
            this.treeView_SoftwareProjects.TabIndex = 1;
            // 
            // contextMenuStrip_UseCases
            // 
            this.contextMenuStrip_UseCases.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createGraphMLToolStripMenuItem});
            this.contextMenuStrip_UseCases.Name = "contextMenuStrip_UseCases";
            this.contextMenuStrip_UseCases.Size = new System.Drawing.Size(171, 26);
            this.contextMenuStrip_UseCases.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip_UseCases_Opening);
            // 
            // createGraphMLToolStripMenuItem
            // 
            this.createGraphMLToolStripMenuItem.Name = "createGraphMLToolStripMenuItem";
            this.createGraphMLToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.createGraphMLToolStripMenuItem.Text = "x_Create GraphML";
            this.createGraphMLToolStripMenuItem.Click += new System.EventHandler(this.createGraphMLToolStripMenuItem_Click);
            // 
            // imageList_UseCases
            // 
            this.imageList_UseCases.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_UseCases.ImageStream")));
            this.imageList_UseCases.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_UseCases.Images.SetKeyName(0, "bb_home_.png");
            this.imageList_UseCases.Images.SetKeyName(1, "cahiers_de_labo.ico");
            this.imageList_UseCases.Images.SetKeyName(2, "Procedures.png");
            this.imageList_UseCases.Images.SetKeyName(3, "edit-find-replace.png");
            // 
            // timer_UseCaseNodes
            // 
            this.timer_UseCaseNodes.Interval = 300;
            this.timer_UseCaseNodes.Tick += new System.EventHandler(this.timer_UseCaseNodes_Tick);
            // 
            // saveFileDialog_Export
            // 
            this.saveFileDialog_Export.Filter = "GraphML files|*.graphml";
            // 
            // UseCaseVusualizer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 578);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "UseCaseVusualizer";
            this.Text = "x_UseCase-Visualizer";
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStripContainer2.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer2.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer2.ContentPanel.ResumeLayout(false);
            this.toolStripContainer2.ResumeLayout(false);
            this.toolStripContainer2.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.contextMenuStrip_UseCases.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton_Close;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ImageList imageList_UseCases;
        private System.Windows.Forms.ToolStripContainer toolStripContainer2;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_SoftwareProjectsLBL;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_SoftwareProjects;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_UseCasesCountLbl;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_UseCasesCount;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar_LoadUseCases;
        private System.Windows.Forms.TreeView treeView_SoftwareProjects;
        private System.Windows.Forms.Timer timer_UseCaseNodes;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip_UseCases;
        private System.Windows.Forms.ToolStripMenuItem createGraphMLToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog_Export;
    }
}

